var context, $canvas, ancho, alto;
function dibujarCirculo(strokeStyle, lineWidth, x, y, rad) {
  context.strokeStyle = strokeStyle;
  context.lineWidth = lineWidth;
  context.beginPath();  // Comienzo a definir el trazo del pincel
  context.arc(x,y,rad,0,Math.PI*2,true);
  context.closePath();  // Declaro el fin del trazo
  context.stroke();     // Le digo al contexto que se dibujen los bordes
}
function dibujarCirculoDifuso(strokeStyle, lineWidth, blur, intervalorDeDifuminado, x, y, rad) {
  context.clearRect(0,0,ancho,alto);
  for (var i = 0; i <= blur; i = i + intervalorDeDifuminado) { dibujarCirculo(strokeStyle, lineWidth + i, x, y, rad); }
}
function dibujarCirculoAnimado(strokeStyle, lineWidth, blur, intervaloDeDifuminado, duracion, x, y, radIni, radFin) {
  var periodo = 30; //ms
  var incrementoDeRadio = radFin - radIni;
  var numeroFotogramas = duracion/periodo;
  var incrementoPixelsPorFotograma = incrementoDeRadio / numeroFotogramas;
  var frameActual = 0;
  $canvas.css('z-index',1);
  var timer = setInterval(function(){
    if (frameActual<numeroFotogramas) {
      dibujarCirculoDifuso(strokeStyle,lineWidth,blur,intervaloDeDifuminado,x,y, radIni + (frameActual * incrementoPixelsPorFotograma));
      frameActual+=1;
      context.globalAlpha = 1.1 - (frameActual/numeroFotogramas);
    } else {  // Finaliza la animación
      context.clearRect(0,0,ancho,alto);
      context.globalAlpha = 1;
      clearInterval ( timer );
      $canvas.css('z-index',-1);    // Finaliza la animacion: hundo el canvas
    }
  },periodo);
}
function animacionDoble(strokeStyle, lineWidth, blur, intervaloDeDifuminado, duracion, x, y, radIni, radFin) {
  var f = (function(){ dibujarCirculoAnimado(strokeStyle, lineWidth, blur, intervaloDeDifuminado, duracion, x, y, radIni, radFin); });
  f();
  setTimeout(f, duracion + 300);
}
function drawRingOnMap(map,lat,lng,radio){
  var projection = (new MyOverlay(map)).getProjection();
  var puntocentral =  projection.fromLatLngToContainerPixel(new GM.LatLng(lat,lng));
  var nuevaLatitud = lat - (radio * 1000 / (1852 * 60));
  var ringRadius = (projection.fromLatLngToContainerPixel(new GM.LatLng(nuevaLatitud,lng))).y - puntocentral.y;
  animacionDoble("rgba(0,0,255,0.05)",5,12,3,400,puntocentral.x,puntocentral.y,ringRadius*0.85, ringRadius*1.1);
}
$(function(){
  if (!!document.createElement('canvas').getContext) {
    var $map = $("#map_canvas");
    $canvas = $('<canvas id="canvas" height="'+$map.height()+'px" width="'+$map.width()+'px"> Canvas </canvas>');
    $canvas.css({position: "absolute", left: $map.offset().left+2, top: $map.offset().top+2, 'z-index': -1 });
    $("#map_canvas").after($canvas);
    context = $canvas[0].getContext("2d");
    ancho = $canvas.width();
    alto = $canvas.height();
  }
});