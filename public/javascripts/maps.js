/******* VARIABLES GLOBALES ******/
var GM = google.maps;   //Para abreviar en el futuro
var photo_sound = null;
window.myGM = { opts : {}, over : {} };
//options
myGM.opts.map = {
  zoom: 13,
  scrollwheel:false,
  center: new GM.LatLng(43.35564,-8.389435),
  mapTypeId: GM.MapTypeId.ROADMAP,
  mapTypeControlOptions: {  style: GM.MapTypeControlStyle.DROPDOWN_MENU }
};
myGM.opts.editMap = $.extend({},myGM.opts.map,{streetViewControl : true});
myGM.opts.sv = {
  position: new GM.LatLng(43.379359,-8.401502),     // No se porque tiene que inicializo a esta posicion
  navigationControl:false,
  pov: { heading: 34, pitch: 10, zoom: 1},
  addressControlOptions: { style: {color: "whitesmoke",background: "rgba(0,0,0,0.7)",border: "1px solid white",'border-radius':"5px",'-moz-border-radius':"5px",'-o-border-radius':"5px" } }
};
myGM.opts.showMarker = {/*Empty*/};
myGM.opts.editMarker = { draggable : true };
myGM.opts.infobox = { closeBoxURL:"", pixelOffset:new GM.Size(-30,-120), enableEventPropagation:true, boxStyle:{ width: 'auto', height: 'auto' } };
//overlays
myGM.over.allMarkers = [];
myGM.over.editMarker = null;
myGM.over.infoBox = null;
//GM Objects
myGM.geocoder = new GM.Geocoder();
// EXTENSION PARA PODER OBTENER EL PIXEL DE PANTALLA CORRESPONDIENTE A UNA COORDENADA
MyOverlay.prototype = new GM.OverlayView();
MyOverlay.prototype.onAdd = function() { };
MyOverlay.prototype.onRemove = function() { };
MyOverlay.prototype.draw = function() { };
function MyOverlay(map) { this.setMap(map); }
/************** FUNCIONES DE EDIT_MAP  *****************/
function saveLatLng(){
  $("#restaurant_lat").val(myGM.over.editMarker.getPosition().lat());
  $("#restaurant_lng").val(myGM.over.editMarker.getPosition().lng());
}
function placeInitialMarker(){
  var lat = $("#restaurant_lat").val();
  if (lat!="") {
    var lng = $("#restaurant_lng").val();
    myGM.map.setCenter(new GM.LatLng(lat,lng));
    myGM.over.editMarker = new GM.Marker($.extend({},myGM.opts.editMarker, {position : new GM.LatLng(lat,lng)}));
    GM.event.addListener(myGM.over.editMarker,'dragend',setEditMarkerAddress);
  }
}
function placeMarkerFromSV(){
  var pos = myGM.map.getStreetView().getPosition();
  var heading = myGM.map.getStreetView().getPov().heading;
  var distance = 0.00012; // Unos pocos metros
  var var_north = Math.sin((90 - heading)*Math.PI/180 )*distance;
  var var_east = Math.cos( (90 - heading)*Math.PI/180 )*distance;
  pos = new GM.LatLng(pos.lat() + var_north ,pos.lng() + var_east);
  newEditMarker({latLng: pos });
}
function getComps(comp){            // :return=>array_con_la_dirección, :formato=>[1-5,calle real,A Coruña,1,ES]
  var comps = [null,null,null,null,null];
  $(comp).each(function(i,e){
    if (e.types[0]=="street_number"){ comps[0] = e.long_name }
    else if (e.types[0]=="route") { comps[1] = e.long_name }
    else if (e.types[0]=="locality") { comps[2] = e.long_name }
    else if (i>0 && e.types[0]=='administrative_area_level_1') {
      if (comp[i-1].types[0]=="administrative_area_level_2") {comps[3] = $.inArray(comp[i-1].long_name,myGM.provinces);}
      else {comps[3] = $.inArray(e.long_name,myGM.provinces) } }
    else if (e.types[0]=="country") { comps[4] = e.short_name }
  });
  return comps;
}
function setEditMarkerAddress(){            // Geocodifica la direccion a partir de la pos del marcador
  myGM.geocoder.geocode({'latLng' : myGM.over.editMarker.getPosition()}, function(results,status) {
    if (status == 'OK'){
      var c = getComps(results[0].address_components);
      if (c[4]=='ES') { // Si no se pincha en españa, no se hace nada
        myGM.over.editMarker['address']=results[0].formatted_address;
        myGM.over.editMarker.setTitle(results[0].formatted_address);
        $.each({street_number: c[0], street_name: c[1], town: c[2], province: c[3] },function(key,value){$("#restaurant_"+key).val(value); });
        $("#restaurant_province").siblings(".selectbox").find(".items li a span.value").each(function(i,e){
          if ($(e).text()==c[3].toString()) { $(e).closest("a").trigger("click"); return false; } });
      }
      else { myGM.over.editMarker.setMap(null); alert('Actualmente solo se admiten locales en España')}
    } else { alert('La geolocalizacion fallo debido a: '+status) }
  });
}
function newEditMarker(e){          // Crea (o modifica) el editmarket
  var opts = $.extend({},myGM.opts.editMarker,{ position : e.latLng } );
  if (myGM.over.editMarker) { myGM.over.editMarker.setOptions( opts ); }
  else { myGM.over.editMarker = new GM.Marker( opts ); GM.event.addListener(myGM.over.editMarker,'dragend',setEditMarkerAddress ) }
  setEditMarkerAddress();
  myGM.map.panTo(myGM.over.editMarker.getPosition());
  saveLatLng();
}
function placeMarkerGeocodingAddress(){            // Coloca un marcador a partir de las direccion, geocodificandola
  var address = [$("#restaurant_street_name").val(),$("#restaurant_street_number").val(),$("#restaurant_town").val(),$("#restaurant_province :selected").text(),"España"].join(", ");
  alert(address);
  myGM.geocoder.geocode({'address' : address,'partialmatch' : true}, function(results,status){
    if (status == 'OK'){ newEditMarker( $.extend({}, myGM.opts.editMarker,{ position : results[0].geometry.location } ) ) }
    else { alert('La geolocalizacion fallo debido a: '+status) }
  });
}
function captureSV(){           // Captura las coordenadas de SV, con efecto foto
  var sv = myGM.map.getStreetView(); var pos = sv.getPosition(); var pov = sv.getPov();
  if (sv.getVisible()) {
    if (photo_sound!=null){ photo_sound.play(); }
    $("#photo_effect").addClass("visible");
    setTimeout(function(){ $("#photo_effect").removeClass("visible"); },100);
    $.each({ lat: pos.lat(), lng: pos.lng(), heading: pov.heading, pitch: pov.pitch },function(key,value){ $("#restaurant_sv_"+key).val(value);});
  } else { alert("Debes tener streetview abierta"); }
}
function setEditMapBehavior(){
  myGM.provinces = $("#restaurant_province option").map(function(i,e){ return $(e).text() }).get();   // Obtengo el array de nombres de provincias
  myGM.map = new GM.Map($("#map_canvas")[0],myGM.opts.editMap);
  myGM.opts.editMarker["map"]=myGM.map;
  if ($('html').hasClass('audio')){ photo_sound = new Audio("/sounds/photo.wav"); }
  GM.event.addListenerOnce(myGM.map,'tilesloaded', placeInitialMarker );                              // Coloca marcador si estamos editando
  GM.event.addListener(myGM.map,'rightclick',function(e){ newEditMarker(e) });                        // RightClick => new marker
  GM.event.addListener(myGM.map.getStreetView(),'visible_changed',function(){
    $(".edit_map_buttons").toggleClass("visible", myGM.map.getStreetView().getVisible() );
  });
  $('#geolocate').click( placeMarkerGeocodingAddress );                                               // Geocodifica direccion y la marca
  $("#capture_sv").click( captureSV );                                                                // Captura el streetview actual
  $("#put_marker").click( placeMarkerFromSV );                                                      // Coloca un marcador en donde se mira en SV
}
/************** FUNCIONES DE SHOW_MAP  *****************/
// Cierra la infobox abierta en ese momento
function closeInfoBox(){
  var ib = myGM.over.infoBox, mkr = $(ib).data('marker');
  mkr.infobox=false;
  myGM.over.infoBox = null;
  $(ib.div_).fadeOut(200,function(){ ib.close(); ib = null; });
}
//Abre un infobox sobre un marcador, con efecto de fadeIn. Cierra cualquier otra infobox abierta
function openInfoBox(mrkr){
  $.get('/restaurants/'+mrkr.id,{infobox:"medium"},function(data){
    if (myGM.over.infoBox) { closeInfoBox(); }
    var borderImageFlag = $('html').hasClass('no-borderimage') ? 'no-borderimage' : '';
    myGM.over.infoBox = new InfoBox($.extend({},myGM.opts.infobox,{content : data, boxClass:("infobox medium"+borderImageFlag)}) );
    $(myGM.over.infoBox).data('marker',mrkr);
    myGM.over.infoBox.open(myGM.map, mrkr);
    GM.event.addListener(myGM.over.infoBox,'domready',function(){$(this.div_).hide().fadeIn(200);});
  });
}
// devuelve un marcador a partir del JSON de un restaurante
function markerFromPlace(place){
  var rest = place.restaurant;
  var marker = new GM.Marker($.extend({},myGM.opts.showMarker,{position:new GM.LatLng(rest.lat,rest.lng), title:rest.name, id:rest.id, infobox:false }));
  GM.event.addDomListener(marker,'click',function() {
    if ( this.infobox ) { closeInfoBox(); this.infobox = false; }
    else { openInfoBox(this); this.infobox = true; } });
  return marker;
}
function addMarkers(markers,substituir){
  if (substituir){ myGM.over.allMarkers=[]; myGM.mgr.clearMarkers();  }
  var ids = $.map(myGM.over.allMarkers,function(e,i){ return e.id; });
  var news = $(markers).filter(function(i){  return ($.inArray(markers[i].id, ids)==-1); });
  myGM.mgr.addMarkers(news,10);         //TODO: esto añade solo las novedades pero no quita nunca marcadores ya añadidos
  $.merge(myGM.over.allMarkers,news);
  if (news.length>0) { myGM.mgr.refresh(); }
}
/****** AUXILIARES ******/
function W3CLocation(){
  if (navigator.geolocation){ navigator.geolocation.getCurrentPosition(function(p){
    myGM.myLocation = new GM.LatLng(p.coords.latitude,p.coords.longitude)}); }
  else { alert('no soportada') }
}
function setUpMap(){
  var $map = $("#map_canvas");
  if ($map.length>0) {
    if ($map.hasClass("edit_map")) { setEditMapBehavior() }
    else {
      if ($map.hasClass("big_map")){
        myGM.map = new GM.Map($map[0],myGM.opts.map);
        setMainMapBehavior(); }
      else {
        var opts = {zoom: $map.data('zoom'),scrollwheel:false,center: new GM.LatLng($map.data('lat'), $map.data('lng')),
          mapTypeId: GM.MapTypeId.ROADMAP,disableDefaultUI: true};
        myGM.map = new GM.Map($map[0],opts);
        new GM.Marker($.extend({map: myGM.map},{position: myGM.map.getCenter()}));
      }
    }
  }
}
// Inventado por mi recientemente
function changeInfoboxSize(size,others){
  $.get("/restaurants/"+$(".infoBoxContent").attr("data-id"),{ infobox: size },function(data){
    if ($("html").hasClass('no-csstransitions')) {
      $(".infoBoxContent").animate({opacity: 0},500,null,function(){
        if ($('.infobox').hasClass('medium')){
          $(".infoBoxContent").html($(data).html());
          $('.infobox').animate({'margin-top': '-173px'},500,null,null);
          $(".infoBoxContent").animate({width: '370px', height: '231px'},500,null,function(){
            $('.infobox').removeClass('medium').addClass('big');
            $(".infoBoxContent").animate({opacity: 1},500,null,function(){ if ($("#streetview_pane").length>0){ startStreetView($("#streetview_pane")); } });
          });
        } else {
          $(".infoBoxContent").html($(data).html());
          $('.infobox').animate({'margin-top': '0'},500,null,null);
          $(".infoBoxContent").animate({width: '230px', height: '58px'},500,null,function(){
            $('.infobox').removeClass('big').addClass('medium');
            $(".infoBoxContent").animate({opacity: 1},500,null,null);
          });
        }
      });
    }
    else {
      $(".infoBoxContent").css("opacity","0");
      $(".infobox").removeClass(others).addClass(size);
      setTimeout(function(){
        $(".infoBoxContent").html($(data).html()).css("opacity","1");
        if ($("#streetview_pane").length>0){ startStreetView($("#streetview_pane")); }},1000);
    }
  });
}
function startStreetView($node) {
  $.extend(true,myGM.opts.sv,{ position: new GM.LatLng($node.data('lat'),$node.data('lng')),
    pov: { heading: parseFloat($node.data('heading')), pitch: parseFloat($node.data('pitch')) } });
  myGM.streetview = new GM.StreetViewPanorama($node[0],myGM.opts.sv);
  setTimeout(function(){
    var temp = $('#streetview_pane span:contains("La dirección es aproximada"),#streetview_pane span:contains("Address is approximate")');
    var $legend = temp.parent("div");
    $legend.addClass("sv_legend");
  },200)
}
function scrollUp(){
  if ( $("#results").attr("data-page")=="0" ){ return;}
  var time = csstransitionsallowed ? 0 : 1000;
  $("#results_inner").animate({"margin-top": "+=351" },time, null,function(){
    $("#results").attr("data-page",parseInt($("#results").attr("data-page"))-1);
    if ($("#results").attr("data-page")=="0") { $("#more_up").removeClass("expanded"); }
    $("#more_down").addClass("expanded");
  });
}
function scrollDown(){
  if ( $("#results").attr("data-page")==$("#results").attr("data-pages") ){ return;}
  var time = csstransitionsallowed ? 0 : 1000;
  $("#results_inner").animate({"margin-top": "-=351" },time, null,function(){
    $("#results").attr("data-page",parseInt($("#results").attr("data-page"))+1);
    if (parseInt($("#results").attr("data-page"))==parseInt($("#results").attr("data-pages"))-1) { $("#more_down").removeClass("expanded"); }
    $("#more_up").addClass("expanded");
  });
}
function expandFilterMenu(){
  if (csstransitionsallowed) { $("#expandible_menu").toggleClass("expanded"); }
  else if ($("#expandible_menu").hasClass("expanded")) {
    $('#filter_form .content').animate({opacity: 0}, 500, null, function(){
      $("#filter_form").animate({width: '5px'},500,null,function(){ $("#expandible_menu").removeClass("expanded"); });
    });
  } else {
    $("#filter_form").animate({width: '200px'},500,null,function(){
      $('#filter_form .content').animate({opacity: 1}, 500, null, function(){ $("#expandible_menu").addClass("expanded"); })
    })
  }
}
/* ***** FUNCIONES DEL MAPA PRINCIPAL ****** */
function setMainMapBehavior(){
  //setTimeout(W3CLocation, 1500); /( Esto ralentiza MUUUUCHO, y no se xq
  $('#tab').click(expandFilterMenu);
  // Cosas de infoboxes
  myGM.mgr = new MarkerManager(myGM.map);                                                                             // Creo un markerManager
  GM.event.addListenerOnce(myGM.map,'idle', function(){  GM.event.trigger(myGM.map, 'dragend') });                                                         // Coloco los marcadores visibles al inicio
  $('.close_infobox').live('click', closeInfoBox );
  $('.to_small_infobox').live('click', function(){ changeInfoboxSize("small","medium big") } );
  $('.to_medium_infobox').live('click', function(){ changeInfoboxSize("medium","small big") } );
  $('.to_big_infobox').live('click', function(){ changeInfoboxSize("big","small medium") } );
  $("#filter_form form").bind('ajax:before',function(){
    if ($(this).attr("data-last-search")==$(this).serialize()) {
      drawRingOnMap(myGM.map,$("#lat").val(),$("#lng").val(),$(this).find('#rad').val());
      return false; }
    $(this).attr("data-last-search",$(this).serialize());
  });
  //Controles
  $("#more_up").click(scrollUp);
  $("#more_down").click(scrollDown);
  $(".more_info").live("click",function(){
    var mkr;var id = $(this).closest(".result").data("restaurant").restaurant.id;
    $(myGM.over.allMarkers).each(function(i,e){ if (e.id==id) { mkr = e; return false; } }) ;
    $(mkr).trigger("click");
    myGM.map.panTo(mkr.getPosition());
  });
  GM.event.addListener(myGM.map,'dragend',function() {
    $("#lat").val(myGM.map.getCenter().lat());
    $("#lng").val(myGM.map.getCenter().lng());
    if ($("#filter_form form").attr("data-last-search")=="") { $("#filter_form form").submit(); }
  }); // Evento que salta al terminar de arrastra el mapa
}
/* ***** MAIN ****** */
$(function(){
  csstransitionsallowed = $('html').hasClass('csstransitions');
  setUpMap();
});