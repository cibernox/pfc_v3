function displaceHighlight(){
  if ($(".highlight").length>0) {
    var index = $("nav.col a[data-active=true]").index("nav.col a");
    $(".highlight").animate({'margin-top' : (-1 + 41*index)+'px' });
  }
}
$(function(){
  // Header
  if ($('html').hasClass('no-csstransitions')) {
    $('#main_nav .item').hover(function(){
                                  var height = $(this).hasClass('login') ? 160 : 130;
                                  $(this).find('.item_content').stop().animate({height: height},300,null,null)},
                              function(){ $(this).find('.item_content').stop().animate({height: '1px'},300,null,null) });
  }
  // Hago visible el resaltador
  $(".highlight").css({visibility : "visible"});
  //Establezco expresamente el ancho y alto de #main_photo
  //$("#main_photo").css({height: $(this).outerHeight()+"px", width: $(this).outerWidth()+"px" });
  // Cambio la foto de muestra por la foto pinchada
  $('#ppy1').popeye({
    caption:    'hover',
    direction:  'left'
  });
});
