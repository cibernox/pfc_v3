function inputsToStars(inputs){
  $(inputs).each(function(i,e){
    $(e).hide().after(function(){
      var $stars = $("<div class='stars'></div>");
      for (var i = 0; i<5;i++) {$stars.append($("<div class='star'><div class='half left'></div><div class='half right'></div></div>"));}
      var value = $(e).val() || 0;
      var $halfs = $stars.find(".half");
      for (var j=0;j<parseInt(value);j++){ $($halfs[j]).addClass("filled") }
      $halfs.each(function(i,e){
        $(e).click(function(evt){
          var $mitades = $(evt.target).closest(".stars").find(".half");
          var position = $mitades.index(evt.target);
          $mitades.each(function(index,elem){ if (index<=position) { $(elem).addClass("filled") } else { $(elem).removeClass("filled") } });
          $(evt.target).closest(".stars").siblings("input").val(position + 1).trigger("mousedown");
        })
      });
      return $stars;
    });
  });
}
function addTicketLine(){
  var $existing = $("#ticket .ticketline[data-course="+ $(this).attr('data-id') +"]");
  if ($existing.length == 0) { $(this).find('.link a').trigger('click'); }
  else {
    var input = $existing.find('.quantity input');
    input.val( parseInt(input.val())+1 );
    $existing.addClass('highlighted');
    sumTicket();
  }
}
function sumTicket(){
  var sum = 0;
  $("#ticket .ticketline").each(function(i,e){ sum += parseFloat($(e).find('.price').html())*parseInt($(e).find('.quantity input').val()) });
  $("#ticket .total .price ").html(Math.round(sum*100)/100).addClass('highlighted');
  setTimeout(function(){ $('.highlighted').removeClass('highlighted'); },200);

}
$(function(){
  // Los enlaces 'disabled' los bloqueo
  $('.disabled').live('click',function(){return false; });
  // Enlaces de la derecha funcionando via AJAX
  $(".bicolumn nav.gray_column a").click(function(){
    $.get(this.href,null,function(data){
      $(".index").fadeOut(400, function(){ $(this).html( $(data).find(".index").html() ).fadeIn(400) })
    });
    history.pushState(null,"",this.href); 
    $(".bicolumn nav.gray_column a").attr('data-active','false');
    $(this).attr('data-active','true');
    setTimeout(displaceHighlight, 300);
    return false;
  });
  // Slides del main
  $(".slide-left").click(function(){
    if ($("#columns .slider").css('margin-left')=='0px') { return false; } //No desplazo
    if ($(this).hasClass('disabled')) { return false; }                    // No desplazo
    $(this).addClass('disabled');
    var time = 0;
    if ($('html').hasClass('no-csstransitions')) { time = 1400 }
    $("#columns .slider").animate({ 'margin-left': '+=320' },time,null,null);
    setTimeout(function(){ $(".slide-left").removeClass('disabled') },1500)
  });
  $(".slide-right").click(function(){
    if ($("#columns .slider").css('margin-left')=='-640px') { return false; }
    if ($(this).hasClass('disabled')) { return false; }
    $(this).addClass('disabled');
    var time = 0;
    if ($('html').hasClass('no-csstransitions')) { time = 1400 }
    $("#columns .slider").animate({ 'margin-left': '-=320' },time,null,null);
    setTimeout(function(){ $(".slide-right").removeClass('disabled') },1500)
  });

  sumTicket();
  $('.tricolumn > .col:nth-child(1)').height( $('.tricolumn > .col:nth-child(2)').outerHeight() + 5);
  $('#close_flash').live('click',function(){
    $(this).closest('#flash').animate({height: '0px'},300, 'linear',
                                     function(){$("#flash").addClass('invisible')})
  });
  //Cerrar flash[:notice] automaticamente  
  setTimeout(function(){
    if ( $('#flash').hasClass('permanent') ) { return false;  }
    else { $('#close_flash').trigger('click') }
  },4000);

  //hago algo de magia con el file field
$("input[type=file]").live("change",function(){
    var path = $(this).val().split("\\");
    var filename = path[path.length - 1];
    $(this).closest('.field').css('opacity','0.5').find('.file_name').text(filename);
    $(this).closest("form").trigger("submit");
  });
  // Desplazo el resaltador( si lo hay) al inicio
  displaceHighlight();
  // Desplazo el resalador y hago focus en el primer elemento del fieldset correspondiente
  $("form .tricolumn nav.col a[data-active]").live("click",function(){
    var $links = $(this).parents("nav.col").find("a[data-active]").attr("data-active",false);
    var index = $links.index($(this).attr("data-active",true));
    var $fieldset = $( $( "section.col.middle fieldset" )[index] );
    var inputs = $fieldset.find(":input");//[0].focus();
    if (inputs.length>0){ inputs[0].focus() }
    else { $fieldset.focus(); }
    $("body:not(:animated)").animate({"scrollTop": $fieldset.offset().top - 400 },300);
    displaceHighlight();
    return false;
  });
  // Desplazo el resaltador al hacer click en un input
  $(".col.middle fieldset :input").live("mousedown",function(){
    var index = $( ".col.middle fieldset" ).index($(this).parents("fieldset"));
    $( $("nav.col a[data-active]").attr("data-active",false).get(index) ).attr("data-active",true) ;
    displaceHighlight();
  });
  //Borrar foto en los formularios
  $(".remove_photo_link").live("click",function(){
    var $field = $(this).parents(".field"); var $photo_fieldset = $field.parents("fieldset");
    $("input:hidden",$field).attr("value","true");
    $field.hide(300);
  });
  // Borrar formulario de nueva foto
  $(".delete_file_field").live('click',function(){ $(this).closest('.field').slideUp(400,function(){ $(this).remove() }) });
  // Personalizo los selectboxes
  // TODO: Desactivado temporal
  $("#filter_form select").sb({  fixedWidth: true  });
  $(".tricolumn select").sb({  fixedWidth: false  });
  $(".selectbox").live("mousedown",function(){ $(this).siblings("select").trigger("mousedown"); });
  // Personalizo los ratings con estrellas
  inputsToStars($("#ratings_fieldset input[type=text]"));
  // Personalizo los checkboxes
  $('#main :checkbox').iphoneStyle({
    checkedLabel:      " SI ",                      // Text content of "on" state
    uncheckedLabel:    " NO "
  });
  // Añadir dinamicamente nuevos platos al restaurante desde las reviews
  $(".delete_nested_course").live("click",function(){ $(this).closest('li.course').slideUp(400,function(){ $(this).remove() }) });
  $(".delete_course").live("click",function(){
    var $form = $(this).closest('form');
    $form.closest('li.course').slideUp(400,function(){
      if ($form.hasClass('new_course')){ $(this).remove() }
      else { return true;}
    })


  });
  $(".save_course").live("click",function(){$.post($(this).closest('form').attr("action") ,$(this).closest('form').serialize(),null,"script") });
  // Enseño el menu en el resumen del restaurante
  $('.show_menu').click(function(){
    if ($('.restaurant_menu').parent().css('display')=='none') { $('.restaurant_menu').parent().slideDown(600);}
    else { $('.restaurant_menu').parent().slideUp(600); }
  });
  // Efectos del menu
  $("html.csstransforms .menu").addClass('csstransforms');
  $("html.csstransforms3d .menu").addClass('csstransforms3d');
  $("html.no-csstransforms3d .menu").addClass('no-csstransforms3d');
  $(".category").click(function(){
    var $clicked = $(this), $cats = $(".category"), pos = $cats.index($clicked);
    var $precedents = $.map($cats,function(e,i){ if ( i<pos ) { return $(e); } });
    var $prev = null; $prev = $($cats[pos-1]);
    var $subsecuents = $.map($cats,function(e,i){ if ( i>pos ) { return $(e); } });
    var $next = null; $next = $($cats[pos+1]);
    $.each($precedents,function(i,e){ $(e).removeClass('right first center').addClass('left');  });
    $.each($subsecuents,function(i,e){ $(e).removeClass('left first center').addClass('right');  });
    $prev.addClass('first');$next.addClass('first');
    $clicked.removeClass('left right first').addClass('center');
    var dx = $clicked.closest('.menu').hasClass('csstransforms3d') ? -76 : -120;
    $clicked.closest(".category_wrapper").css('margin-left',(pos*dx)+'px');
  });
// Ya no va por invisibilidad, va por afecto 3d
  $(".cat-link").click(function(){ $(".category[data-cat="+$(this).attr('data-cat') +"]").trigger('click') });
  // Añadir o modificar ticketlines
  $("#ticketlines_fieldset .course").live("dblclick",addTicketLine);
  $(".add_ticketline_link").live('ajax:complete',function(){ $('.highlighted').removeClass('highlighted'); });
  $("#ticketlines_fieldset .quantity input[type=text]").live('focusout',sumTicket);
  // arrastrado
  $(".course[draggable=true]")
        .bind('dragstart', function(ev) {
            var dt = ev.originalEvent.dataTransfer;
            dt.setData('Text', this.outerHTML);
            $('#ticket').addClass('dragover');
            return true;})
        .bind('dragend', function(ev) {
            $('#ticket').removeClass('dragover');
            return false;});

    // DropZone event
    $("#ticket")
        .bind('dragenter', function(ev) { return false; })
        .bind('dragleave', function(ev) { return false; })
        .bind('dragover', function(ev) { return false; })
        .bind('drop', function(ev) {
            var dt = ev.originalEvent.dataTransfer;
            $(".course[data-id="+ $(dt.getData('Text')).attr('data-id')+"]").trigger('dblclick');
            return false;});
  // Convertir radiobuttons en otra cosa
  $('.radio_label.first').click(function(){
    $(this).siblings(':radio.first').attr('checked',true);
    $(this).addClass('selected'); $(this).siblings().removeClass('selected');
  });
  $('.radio_label.last').click(function(){
    $(this).siblings(':radio.last').attr('checked',true);
    $(this).addClass('selected'); $(this).siblings().removeClass('selected');
  });
  $('.radio_label').not('.first').not('.last').click(function(){
    $(this).siblings(':radio').not('.first').not('.last').attr('checked',true);
    $(this).addClass('selected'); $(this).siblings().removeClass('selected');
  });

});