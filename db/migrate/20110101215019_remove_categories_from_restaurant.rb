class RemoveCategoriesFromRestaurant < ActiveRecord::Migration
  def self.up
    remove_column :restaurants, :main_food
    remove_column :restaurants, :second_food
  end

  def self.down
    add_column :restaurants, :main_food, :integer
    add_column :restaurants, :second_food, :integer
  end
end
