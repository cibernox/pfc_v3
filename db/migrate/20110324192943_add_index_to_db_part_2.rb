class AddIndexToDbPart2 < ActiveRecord::Migration
  def self.up
    add_index :reviews, :user_id
    add_index :reviews, :restaurant_id

    add_index :restaurants, :user_id

    add_index :courses, :restaurant_id

    add_index :authentications, :user_id
  end

  def self.down
  end
end
