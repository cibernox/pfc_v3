class AddAvgPriceToReviews < ActiveRecord::Migration
  def self.up
    add_column :reviews, :avg_price, :float
  end

  def self.down
    remove_column :reviews, :avg_price
  end
end
