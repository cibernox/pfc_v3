class RemoveAvgPriceFromReview < ActiveRecord::Migration
  def self.up
    remove_column :reviews, :avg_price
  end

  def self.down
    add_column :reviews, :avg_price, :float
  end
end
