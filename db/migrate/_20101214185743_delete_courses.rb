class DeleteCourses < ActiveRecord::Migration
  def self.up
    drop_table :courses
  end

  def self.down
  end
end
