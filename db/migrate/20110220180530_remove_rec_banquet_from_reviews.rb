class RemoveRecBanquetFromReviews < ActiveRecord::Migration
  def self.up
    remove_column :reviews, :rec_banquet
  end

  def self.down
    add_column :reviews, :rec_banquet, :integer, :null => false, :default => 0
  end
end
