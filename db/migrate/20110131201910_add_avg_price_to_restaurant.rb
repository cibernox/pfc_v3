class AddAvgPriceToRestaurant < ActiveRecord::Migration
  def self.up
    add_column :restaurants, :avg_price, :float
  end

  def self.down
    remove_column :restaurants, :avg_price
  end
end
