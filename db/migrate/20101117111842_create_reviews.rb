class CreateReviews < ActiveRecord::Migration
  def self.up
    create_table :reviews do |t|
      t.integer :user_id
      t.integer :restaurant_id
      t.decimal :price
      t.integer :customer_type
      t.integer :food_rating
      t.integer :service_rating
      t.integer :local_rating
      t.integer :clean_rating
      t.integer :quality_price_rating
      t.integer :general_rating
      t.boolean :rec_alone
      t.boolean :rec_couple
      t.boolean :rec_child
      t.boolean :rec_friends
      t.boolean :rec_business
      t.boolean :rec_banquet
      t.boolean :rec_gourmets
      t.string :title
      t.text :opinion

      t.timestamps
    end
  end

  def self.down
    drop_table :reviews
  end
end
