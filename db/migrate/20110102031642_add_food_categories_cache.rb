class AddFoodCategoriesCache < ActiveRecord::Migration
  def self.up
    add_column :restaurants, :first_cat, :float
    add_column :restaurants, :second_cat, :float
    add_column :restaurants, :third_cat, :float
  end

  def self.down
    remove_column :restaurants, :first_cat
    remove_column :restaurants, :second_cat
    remove_column :restaurants, :third_cat
  end
end
