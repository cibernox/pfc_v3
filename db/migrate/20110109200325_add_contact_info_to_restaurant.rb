class AddContactInfoToRestaurant < ActiveRecord::Migration
  def self.up
    add_column :restaurants,:tlf,:integer
    add_column :restaurants,:email,:string
    add_column :restaurants,:web,:string
  end

  def self.down
    remove_column :restaurants,:tlf
    remove_column :restaurants,:email
    remove_column :restaurants,:web
  end
end
