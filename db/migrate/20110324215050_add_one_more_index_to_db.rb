class AddOneMoreIndexToDb < ActiveRecord::Migration
  def self.up
    add_index :courses, :user_id
  end

  def self.down
  end
end
