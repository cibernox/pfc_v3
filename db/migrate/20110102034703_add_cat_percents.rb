class AddCatPercents < ActiveRecord::Migration
  def self.up
    remove_column :restaurants, :first_cat
    remove_column :restaurants, :second_cat
    remove_column :restaurants, :third_cat
    add_column :restaurants, :first_cat, :integer
    add_column :restaurants, :second_cat, :integer
    add_column :restaurants, :third_cat, :integer
    add_column :restaurants, :first_cat_ratio, :float
    add_column :restaurants, :second_cat_ratio, :float
    add_column :restaurants, :third_cat_ratio, :float
  end

  def self.down
    remove_column :restaurants, :first_cat
    remove_column :restaurants, :second_cat
    remove_column :restaurants, :third_cat
    remove_column :restaurants, :first_cat_ratio
    remove_column :restaurants, :second_cat_ratio
    remove_column :restaurants, :third_cat_ratio
    add_column :restaurants, :first_cat, :float
    add_column :restaurants, :second_cat, :float
    add_column :restaurants, :third_cat, :float

  end
end
