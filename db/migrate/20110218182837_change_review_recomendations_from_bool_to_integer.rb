class ChangeReviewRecomendationsFromBoolToInteger < ActiveRecord::Migration
  def self.up
    change_column :reviews, :rec_alone,     :integer, :null => false, :default => 0
    change_column :reviews, :rec_banquet,   :integer, :null => false, :default => 0
    change_column :reviews, :rec_business,  :integer, :null => false, :default => 0
    change_column :reviews, :rec_child,     :integer, :null => false, :default => 0
    change_column :reviews, :rec_couple,    :integer, :null => false, :default => 0
    change_column :reviews, :rec_friends,   :integer, :null => false, :default => 0
    change_column :reviews, :rec_gourmets,  :integer, :null => false, :default => 0
  end

  def self.down
    change_column :reviews, :rec_alone,     :boolean, :null => false, :default => false
    change_column :reviews, :rec_banquet,   :boolean, :null => false, :default => false
    change_column :reviews, :rec_business,  :boolean, :null => false, :default => false
    change_column :reviews, :rec_child,     :boolean, :null => false, :default => false
    change_column :reviews, :rec_couple,    :boolean, :null => false, :default => false
    change_column :reviews, :rec_friends,   :boolean, :null => false, :default => false
    change_column :reviews, :rec_gourmets,  :boolean, :null => false, :default => false
  end
end
