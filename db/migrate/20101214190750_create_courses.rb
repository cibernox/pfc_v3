class CreateCourses < ActiveRecord::Migration
  def self.up
    create_table :courses do |t|
      t.integer :restaurant_id
      t.string :name
      t.decimal :price, :precision => 6, :scale => 2
      t.integer :category

      t.timestamps
    end
  end

  def self.down
    drop_table :courses
  end
end
