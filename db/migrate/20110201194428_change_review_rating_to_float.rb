class ChangeReviewRatingToFloat < ActiveRecord::Migration
  def self.up
    change_column :reviews, :food_rating, :float
    change_column :reviews, :service_rating, :float
    change_column :reviews, :local_rating, :float
    change_column :reviews, :clean_rating, :float
    change_column :reviews, :quality_price_rating, :float
    change_column :reviews, :general_rating, :float
  end

  def self.down
    change_column :reviews, :food_rating, :integer
    change_column :reviews, :service_rating, :integer
    change_column :reviews, :local_rating, :integer
    change_column :reviews, :clean_rating, :integer
    change_column :reviews, :quality_price_rating, :integer
    change_column :reviews, :general_rating, :integer
  end
end

