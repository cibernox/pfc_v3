class ChangeRestaurantTownToString < ActiveRecord::Migration
  def self.up
    remove_column :restaurants, :town
    add_column :restaurants, :town, :string
  end

  def self.down
    add_column :restaurants, :town,:integer
    remove_column :restaurants, :town
  end
end
