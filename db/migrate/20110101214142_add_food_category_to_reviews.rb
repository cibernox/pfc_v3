class AddFoodCategoryToReviews < ActiveRecord::Migration
  def self.up
    add_column :reviews, :main_food, :integer
    add_column :reviews, :second_food, :integer
  end

  def self.down
     remove_column :users, :main_food
     remove_column :users, :second_food
  end
end
