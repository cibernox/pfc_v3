class CreateRestaurants < ActiveRecord::Migration
  def self.up
    create_table :restaurants do |t|
      t.string :name
      t.float :lat
      t.float :lng
      t.float :sv_lat
      t.float :sv_lng
      t.float :sv_heading
      t.float :sv_pitch
      t.integer :province
      t.integer :town
      t.string :street_name
      t.string :street_number
      t.integer :main_food
      t.integer :second_food
      t.boolean :celiac
      t.boolean :adapted
      t.boolean :take_away
      t.boolean :delivery
      t.boolean :credit_card
      t.boolean :menu

      t.timestamps
    end
  end

  def self.down
    drop_table :restaurants
  end
end
