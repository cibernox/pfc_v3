class ChangeTlfToString < ActiveRecord::Migration
  def self.up
    remove_column :restaurants,:tlf
    add_column :restaurants,:tlf,:string
  end

  def self.down
    remove_column :restaurants,:tlf
    add_column :restaurants,:tlf,:integer
  end
end
