class AddIndexToDb < ActiveRecord::Migration
  def self.up
    add_index :ticketlines, :review_id
    add_index :ticketlines, :course_id
  end

  def self.down
  end
end
