class AddGeometriesToPhoto < ActiveRecord::Migration
  def self.up
    add_column :photos, :medium_geometry, :string
    add_column :photos, :big_geometry, :string
    add_column :photos, :biggest_geometry, :string
    add_column :photos, :original_geometry, :string
  end

  def self.down
    remove_column :photos, :medium_geometry
    remove_column :photos, :big_geometry
    remove_column :photos, :biggest_geometry
    remove_column :photos, :original_geometry
  end
end
