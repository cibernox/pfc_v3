class AddLotsOfFieldsToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :name, :string
    add_column :users, :ap1, :string
    add_column :users, :ap2, :string
    add_column :users, :born_on, :date
    add_column :users, :province, :integer
    add_column :users, :town, :string
    add_column :users, :loved_food, :integer
    add_column :users, :hated_food, :integer
  end

  def self.down
    remove_column :users, :hated_food
    remove_column :users, :loved_food
    remove_column :users, :town
    remove_column :users, :province
    remove_column :users, :born_on
    remove_column :users, :ap2
    remove_column :users, :ap1
    remove_column :users, :name
  end
end
