# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110324215050) do

  create_table "authentications", :force => true do |t|
    t.integer   "user_id"
    t.string    "provider"
    t.string    "uid"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  add_index "authentications", ["user_id"], :name => "index_authentications_on_user_id"

  create_table "courses", :force => true do |t|
    t.integer   "restaurant_id"
    t.string    "name"
    t.decimal   "price"
    t.integer   "category"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.integer   "user_id"
  end

  add_index "courses", ["restaurant_id"], :name => "index_courses_on_restaurant_id"
  add_index "courses", ["user_id"], :name => "index_courses_on_user_id"

  create_table "photos", :force => true do |t|
    t.integer   "attachable_id"
    t.string    "attachable_type"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "image_file_name"
    t.string    "image_content_type"
    t.integer   "image_file_size"
    t.timestamp "image_updated_at"
    t.string    "medium_geometry"
    t.string    "big_geometry"
    t.string    "biggest_geometry"
    t.string    "original_geometry"
  end

  create_table "restaurants", :force => true do |t|
    t.string    "name"
    t.float     "lat"
    t.float     "lng"
    t.float     "sv_lat"
    t.float     "sv_lng"
    t.float     "sv_heading"
    t.float     "sv_pitch"
    t.integer   "province"
    t.string    "street_name"
    t.string    "street_number"
    t.boolean   "celiac"
    t.boolean   "adapted"
    t.boolean   "take_away"
    t.boolean   "delivery"
    t.boolean   "credit_card"
    t.boolean   "menu"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "town"
    t.integer   "user_id"
    t.integer   "first_cat"
    t.integer   "second_cat"
    t.integer   "third_cat"
    t.float     "first_cat_ratio"
    t.float     "second_cat_ratio"
    t.float     "third_cat_ratio"
    t.string    "email"
    t.string    "web"
    t.string    "tlf"
    t.float     "avg_price"
  end

  add_index "restaurants", ["user_id"], :name => "index_restaurants_on_user_id"

  create_table "reviews", :force => true do |t|
    t.integer  "user_id"
    t.integer  "restaurant_id"
    t.decimal  "price"
    t.integer  "customer_type"
    t.float    "food_rating"
    t.float    "service_rating"
    t.float    "local_rating"
    t.float    "clean_rating"
    t.float    "quality_price_rating"
    t.float    "general_rating"
    t.integer  "rec_alone",            :default => 0, :null => false
    t.integer  "rec_couple",           :default => 0, :null => false
    t.integer  "rec_child",            :default => 0, :null => false
    t.integer  "rec_friends",          :default => 0, :null => false
    t.integer  "rec_business",         :default => 0, :null => false
    t.integer  "rec_gourmets",         :default => 0, :null => false
    t.string   "title"
    t.text     "opinion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "main_food"
    t.integer  "second_food"
  end

  add_index "reviews", ["restaurant_id"], :name => "index_reviews_on_restaurant_id"
  add_index "reviews", ["user_id"], :name => "index_reviews_on_user_id"

  create_table "ticketlines", :force => true do |t|
    t.integer   "review_id"
    t.integer   "course_id"
    t.integer   "quantity"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  add_index "ticketlines", ["course_id"], :name => "index_ticketlines_on_course_id"
  add_index "ticketlines", ["review_id"], :name => "index_ticketlines_on_review_id"

  create_table "users", :force => true do |t|
    t.string    "email",                               :default => "", :null => false
    t.string    "encrypted_password",   :limit => 128, :default => "", :null => false
    t.string    "password_salt",                       :default => "", :null => false
    t.string    "reset_password_token"
    t.string    "remember_token"
    t.timestamp "remember_created_at"
    t.integer   "sign_in_count",                       :default => 0
    t.timestamp "current_sign_in_at"
    t.timestamp "last_sign_in_at"
    t.string    "current_sign_in_ip"
    t.string    "last_sign_in_ip"
    t.string    "username"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "name"
    t.string    "ap1"
    t.string    "ap2"
    t.date      "born_on"
    t.integer   "province"
    t.string    "town"
    t.integer   "loved_food"
    t.integer   "hated_food"
    t.boolean   "admin"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
