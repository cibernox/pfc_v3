#Comandos para GIT
#  añadir cambios
git add . # todos los cambios en el directorio actual
git commit -m 'Mensaje_del_commit'  #crea un commit para subir
git push  # para subir los cambios a github
git push heroku master  #subir los cambios a heroku

## Comandos que ejecuto uno tras otro para crear el proyecto
rails new v3 -d mysql
cd v3
#para iniciar el servidor de sql, si no funciona desde las preferencias, este comando:
sudo /Library/StartupItems/MySQLCOM/MySQLCOM start
#y para pararlo
sudo /Library/StartupItems/MySQLCOM/MySQLCOM stop
# creo las tablas
rake db:create:all
# añado las gemas al GEMFILE
  #gem 'mysql2'
  #gem 'haml'
  #gem 'devise'
  #gem 'hpricot'
  #gem 'ruby_parser'
  #gem 'omniauth'
bundle install
# modifico config/applicattion.rb
	config.generators do |g|
      	g.template_engine :haml
  end
#copio el repositorio de generadores
git clone git://github.com/psynix/rails3_haml_scaffold_generator.git lib/generators/haml
#instalo devise
rails g devise:install
# hago los ajustes manuales
 # 1 - Configurar host del mail en development.rb
 # 2 - Configurar un ruta root en routes.rb (borrando o renombrando public/index.html)
 # 3 - Añado los notices y alerts al layout (y transformo el layout a haml)
# Creo el usuario
rails g devise User
# Le voy a añadir el campo username, pero quedan muchos por añadir
  # 1 - Modifico la migración creada para meter el username, y creo tambien un attr_accessible
  #     en el modelo para ese campo
# Ejecuto la migracion
rake db:migrate
# Genero las vistas de devise para poder alterarlas luego si quiero
rails g devise:views

##  CONFIGURACION DE OMNIAUTH  ##
# 1 - CREO EL ARCHIVO ./config/initializers/omniauth.rb
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, 'URJ3ZecYdqShdWmPqoEXAA', 'XnT6iysG68imfYoTv15npgEgm9Yj7ueeK7820MmCQM'
end
# 2 - Voy a dev.twitter.com para registrar una nueva aplicación.
  # Registro mi aplicacion con el dominio que vaya a tener
  # en produccion para poder aprovecharlo. Si no lo hago, luego tendre que dar de alta otra aplicación o editar esta.
  # En cualquier caso, yo no lo hice y tngo que acordarme >>> TODO: Cambiar la configuracion en dev.twitter.com en produccion
  # Si estamos en development la direccion de callback es un problema porque twitter no nos deja poner localhost.
  # Parche sencillo para solucionarlo: Con bit.ly crear una direccion acordadora de http://localhost:3000/twitter_callback

  # http://bit.ly/FPigp => Esta funciona

  # La segunda y mejor opcion es poner http:127.0.0.1/twitter_callback . Es absurdo, pero localhost no deja y esto si.

  # Comprobar que la direccion localhost:3000/auth/twitter nos intenta autorizar. Puede que si tenemos marcado en el navegador
  # que recuerde la contraseña nos parezca que no lo hace, pero si la respuesta nos intenta redirigirnos a /auth/twitter/callback
  # (que de momento no existe) es que la redirección funciona.
  # Importante: Si en lugar de ir a /auth/twitter vamos a /auth/twitter/ no funciona, no es lo mismo
# 3 - Creo un modelo Authentication con acciones index, create y destroy. Completo, con scaffold.
rails g scaffold authentication user_id:integer provider:string uid:string # solo index create destroy
# y migro
rake db:migrate
  # y establezco la cardinalidad de la relacion en los modelos
class User < ActiveRecord::Base
  has_many :authentications
end
class Authentication < ActiveRecord::Base
  belongs_to :user
end
#segir el tutorial de railscast/asciicast


## AMPLIACION DEL MODELO DE USUARIO ##
rails g migration add_lots_of_fields_to_user name:string ap1:string ap2:string born_on:date province:integer town:string loved_food:integer hated_food:integer
## Migración para crear los restaurantes
rails g scaffold restaurant name:string lat:float lng:float sv_lat:float sv_lng:float sv_heading:float sv_pitch:float province:integer town:integer street_name:string street_number:string main_food:integer second_food:integer celiac:boolean adapted:boolean take_away:boolean delivery:boolean credit_card:boolean menu:boolean

## TOCA CREAR LAS REVIEWS
rails g scaffold review user_id:integer restaurant_id:integer price:decimal customer_type:integer food_rating:integer service_rating:integer local_rating:integer clean_rating:integer quality_price_rating:integer general_rating:integer rec_alone:boolean rec_couple:boolean rec_child:boolean rec_friends:boolean rec_business:boolean rec_banquet:boolean rec_gourmets:boolean title:string opinion:text
## Me equivoque al crear los restaurantes. Town es string, no integer, migración para arreglarlo
...
# para instalar imageMagick hay muchas dependencias. Si nos da un error con unas cabeceras de Java, ejecutar este comando
sudo ln -s /Developer/SDKs/MacOSX10.6.sdk/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Headers /System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Headers
## Creo el modelo foto, en principio solo con un atributo título para testear que funcionan las relaciones polimórficas
rails g scaffold photo title:string attachable_id:integer attachable_type:string
# Migro
rake db:migrate
# en Photo.rb
belongs_to :attachable, :polymorphic => true
# En User, Restaurant y Review
has_many :photos, :as => :attachable
accepts_nested_attributes_for :photos, :allow_destroy => true
# Ahora introduzco el concepto de plato
rails g model course review_id:integer name:string price:decimal
rake db:migrate
# me he dado cuenta de que vale la pena salvar las dimensiones de las fotos guardadas
rails g migration add_geometries_to_photo
def self.up
    add_column :photos, :medium_geometry, :string
    add_column :photos, :big_geometry, :string
    add_column :photos, :biggest_geometry, :string
    add_column :photos, :original_geometry, :string
  end

  def self.down
    remove_column :photos, :medium_geometry
    remove_column :photos, :big_geometry
    remove_column :photos, :biggest_geometry
    remove_column :photos, :original_geometry
  end
# llegado a este punto me doy cuenta de que tengo el concepto de plato totalmente mal planteado
# Ahora lo voy a plantear de esta manera
# Un restaurante tiene platos, los cuales pertenecen a un único restaurante (aunque 2 restaurantes tengan el mismo plato,
# se consideran platos distintitos, pues pueden tener distinto precio). Además ese plato tiene tiene una categoria (un integer)
# que indicará si es un entrante, un primero, un segundo, bebida, vino (Igual va mejor algo tipo carnes, pescados, heuevos, pasta....)
# Por otro lado, las reviews tiene asociado un ( y solo un ) ticket. Ese ticket pertenece a ESA review.
# Cada tickets tiene N lineas (tipica factura-lineas_de_factura).
# Cada linea tiene un ticket padre, un plato asociado, y una cantidad.
rails g migration delete_courses
  def self.up
    drop_table :courses
  end
# creo la nueva estructura de courses
rails g scaffold course restaurant_id:integer name:string price:decimal category:integer
# borro controller y views porque ni falta que hacen
# creo las lineas, porque el tickets es una entidad ficticia, no tiene sentido real
rails g scaffold ticketline review_id:integer course_id:integer quantity:integer
# tambien me doy cuenta de que los restaurantes tienen que tener un campo que identifique a su autor, para que solo
# él, o un administrador, pueda editarlo o borrarlo
rails g migration add_user_to_restaurant


