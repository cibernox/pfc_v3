require 'test_helper'

class TicketlinesControllerTest < ActionController::TestCase
  setup do
    @ticketline = ticketlines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ticketlines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticketline" do
    assert_difference('Ticketline.count') do
      post :create, :ticketline => @ticketline.attributes
    end

    assert_redirected_to ticketline_path(assigns(:ticketline))
  end

  test "should show ticketline" do
    get :show, :id => @ticketline.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @ticketline.to_param
    assert_response :success
  end

  test "should update ticketline" do
    put :update, :id => @ticketline.to_param, :ticketline => @ticketline.attributes
    assert_redirected_to ticketline_path(assigns(:ticketline))
  end

  test "should destroy ticketline" do
    assert_difference('Ticketline.count', -1) do
      delete :destroy, :id => @ticketline.to_param
    end

    assert_redirected_to ticketlines_path
  end
end
