# coding: UTF-8
module ModelsHelper
  extend ActiveSupport::Concern
  include AdminHelper
  # INSTANCE METHODS
  # CLASS METHODS
  module ClassMethods
    def search(search, column, direction)
      where(build_exceptions(search)).where(build_condition(search)).order(column ? "#{column} #{direction}" : "")
    end
    def build_condition(search)
      return '1=1' unless search
      search.collect { |key, value| !%w{rad lat lng}.include?(key) ? (columns_hash[key].type==:string ? "#{key} like '%#{value}%'" : "#{key}==#{value}") : nil }.compact.join(" AND ")
    end
    def build_exceptions(search)
      return '1=1' unless search
      search.delete_if{ |key, value| value=='' || value=='0' }
      res = []
      search.each do |key,value|
        res << case key
                 when 'restaurant_name'
                   "restaurant_id in (" + Restaurant.where("name like '%#{search[:restaurant_name]}%'").map { |r| r.id}.join(',')+")"
                 when 'review_title'
                   "review_id in (" + Review.where("title like '%#{search[:review_title]}%'").map { |r| r.id}.join(',')+")"
                 when 'course_name'
                   "course_id in (" + Course.where("name like '%#{search[:course_name]}%'").map { |r| r.id}.join(',')+")"
                 else
                   '1=1'
               end if %w[restaurant_name review_title course_name].include?(key)
      end
      search.delete_if{ |key, value| %w[restaurant_name review_title course_name].include?(key)}
      res
    end
  end

end