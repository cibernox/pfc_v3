module UsersHelper
  def user_avatar(user,style, size=nil)
    image_tag (user.photos[0] ? user.photos[0].image.url(style) : "no-avatar.png"), :size=>size
  end
end
