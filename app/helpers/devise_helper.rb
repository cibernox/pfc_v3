module DeviseHelper
  def devise_error_messages1!
    resource.errors.full_messages.map { |msg| content_tag(:li, msg) }#.join
  end
  def devise_errors!
    error_msgs = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }
    unless error_msgs.size==0
      flash[:devise]='Los siguientes errores impidieron el registro:'
      error_msgs.each_with_index {|m,i| flash['devise'+i.to_s]=m  }
      session[:permanent]=true
    end
  end
end