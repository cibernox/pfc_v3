# coding: UTF-8
module ApplicationHelper

  def label_value_tag(label,value)
    content_tag :div, :class=>'label-value full' do
      content_tag(:div, label_tag(label), :class=>'label') + (content_tag :div, value, :class=>'value ellipsis-text')
    end
  end

  def show_stars(rating, label, html_class=nil)
    rat = rating.round.to_i
    content_tag :div, :class=>"full show stars #{html_class}" do
      content_tag( :div, label_tag(t(label)), :class=>(label ? 'label' : 'hidden') ) +
      (content_tag :div,:class=>'stars_list' do
        "#{(rat/2).times { concat image_tag('iconos/star_mini.png')}} #{ concat image_tag('iconos/star_mini_yellow_gray.png')  if rat%2==1} #{(5 - (rat/2+rat%2)).times { concat image_tag "iconos/star_mini_gray.png" }}"
      end)
    end
  end

  def centered_submit_button(form)
    content_tag(:div, :class=>'buttons center-text') { content_tag :div, form.submit("Guardar #{t form.object.class.name.downcase}",:class=>'default_button'), :class=>'button' }
  end

  def label_with_errors(form,field,errors)
    (form.label field, t(field).titleize) + (errors[field].collect {|e| content_tag :div, e.humanize, :class=>'explanation error_explanation' } unless errors[field].empty? )
  end

  def image_tag_sized(photo,style, options={})
    image_tag photo.image.url(style),options.merge({ 'data-height' => photo.height(style), 'data-width' => photo.width(style) })
  end

  def appropriate_input(form,field,value)
    case form.object.class.columns_hash[field.to_s].type
      when :string,:float,:decimal  then form.text_field field, :value => value
      when :integer
        if COLLECTIONS[field]
          form.collection_select field, order_hash(COLLECTIONS[field]),:last,:first, {:selected=>value},{:class=>'select_form white'}
        else
          (form.radio_button(field, '-1', :class=>'hidden first') + form.label("#{field}_-1", 'No',:class=>"radio_label first #{'selected' if value==-1}") + form.radio_button(field,'0',:class=>'hidden') + form.label("#{field}_0", 'Tal vez',:class=>"radio_label #{'selected' if value==0}") +
          form.radio_button(field,'1',:class=>'hidden last') + form.label("#{field}_1", 'Sí',:class=>"radio_label last #{'selected' if value==1}") ) if field.to_s.starts_with? 'rec_'
        end
      when :boolean then form.check_box field, :value=>value
      when :date    then form.date_select field, {:order =>[:day,:month,:year], :start_year=>1940, :end_year=>Time.now.year ,:value => value},{:class=>'select_form white select_'+field.to_s}
      when :text    then form.text_area field, :value => value
      else value.class.name
    end
  end

  def order_hash(hsh)
    hsh.collect{|c| [c.last , c.first] }.sort {|a,b| a[1] <=> b[1]}
  end

  def nested_text_field_for(parent_class_name,object,index,attr,placeholder)
    nested = object.class.table_name
    id = "#{parent_class_name}_#{nested}_attributes_#{index.to_s}_#{attr.to_s}"
    name = "#{parent_class_name}[#{nested}_attributes][#{index.to_s}][#{attr.to_s}]"
    text_field_tag id,object[attr],{:name=>name,:placeholder=>placeholder}
  end

  def nested_hidden_field_for(parent_class_name,object,index,attr,value=nil)
    nested = object.class.table_name
    id = "#{parent_class_name}_#{nested}_attributes_#{index.to_s}_#{attr.to_s}"
    name = "#{parent_class_name}[#{nested}_attributes][#{index.to_s}][#{attr.to_s}]"
    tag(:input,{:id=>id,:name=>name, :type=>:hidden,:size=>"30",:value=>(value || object[attr]) })
  end

end
