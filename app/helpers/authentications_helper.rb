module AuthenticationsHelper

  def delete_omniauth
    session[:omniauth] = nil
  end
end
