module AdminHelper

  def filter_zeros_from_eq_params
    params[:search].each do |param|
      params[:search].delete(param[0]) if param[0].match(/(_eq)$/) && param[1]=='0'
    end if params[:search]
  end

  def filter_field_label(form,field)
    form.label field, t( field.to_s.split('_').first ).titleize
  end

  def filter_field_input(form, field)
    splt = field.to_s.split('_')
    case splt.last
      when 'eq' then return form.collection_select field, order_hash(COLLECTIONS[splt.first.to_sym]),:last,:first,{:selected => 0},{ :class=>'select_form white'}
      else return form.text_field field
    end
  end

  def get_table_field_value(model, field)
    return model.send(field) unless [:province, :category].include?(field)
    case field
      when :province then PROVINCES[model[field]]
      when :category then FOOD_CATEGORIES[model[field]]
      else "otro"
    end
  end

end