class CoursesfieldsetCell < FieldsetCell
  def menu(restaurant = nil)
    @restaurant = restaurant || get_restaurant
    render
  end
  def only_menu(args)
    @restaurant = args[:restaurant]
    render :view => :menu
  end

  def category_card(index, name)
    @name = name
    @cat_index = index
    @cat_class = "#{index==1 ? 'center' : 'right'} #{'first' if index==2}"
    @cat_courses = @restaurant.category_courses(index)
    render
  end

  def course(c, form = nil)
    @course = c
    @form = form if form
    @review_id = form && @form.object.class == Review ? @form.object.id : nil
    @draggable = form && @form.object.class == Review ? 'true' : nil
    render
  end

  def ending
    @review = @form.object.class == Review ? @form.object : nil
    render if @review
  end
  def ticketline(t)
    @tline = t
    render
  end

  def get_restaurant
    @form.object.class == Restaurant ? @form.object : @form.object.restaurant
  end
end
