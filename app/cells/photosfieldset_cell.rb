class PhotosfieldsetCell < FieldsetCell
  def show(args)
    @form     = args[:form]
    @section  = args[:section]
    @fields   = @form.object.photos
    render
  end

  def field(fld,index)
    @photo = fld
    render
  end

  def ending
    @attach_id = @form.object.id
    @attach_type = @form.object.class.name
    render
  end

end
