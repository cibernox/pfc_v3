class TricolumnFormCell < Cell::Rails

  def nav_column(sections = nil)
    @sections = sections || @sections || @form.object.class.fieldsets.keys
    @on_admin = defined?(@controller) ? @controller=='admin' : false
    render
  end

  def restaurant_header
    @restaurant = @form.object.class == Restaurant ? @form.object : (@form.object.respond_to?(:restaurant) ? @form.object.restaurant : nil)
    render if @restaurant
  end

  def only_nav_column(args)
    puts "sections => #{args[:sections]}"
    @sections = args[:sections].map { |s| s.to_s}
    @on_admin = args[:controller] == 'admin'
    render :view=>:nav_column
  end

  def show(args)
    @form = args[:form]
    @sections = @form.object.class.fieldsets.keys
    @controller = args[:controller]
    render
  end

end
