class FieldsetCell < Cell::Rails
  helper ApplicationHelper
  build do |args|
    case args[:section]
      when :geolocation then MapfieldsetCell
      when :services,:ratings,:recomendations then BicolumnfieldsetCell
      when :courses then CoursesfieldsetCell
      when :photos then PhotosfieldsetCell
      else FieldsetCell
    end
  end
  # STATES
  def show(args)
    @form     = args[:form]
    @section  = args[:section]
    @fields   = @form.object.class.fieldsets[@section]
    render
  end

  def field(fld,index)
    @field = fld
    @errors   = @form.object.errors
    @value    = @fields.map{ |f| @form.object.send(f) }[index]
    @field_class = field_class
    @input_class = @form.object.class.columns_hash[@field.to_s].type
    render
  end

  def header
    @explanation = @form.object.class.fieldsets_explanations[@section]
    render
  end

  def ending
    @section==:general ? render : ''
  end
  # HELPERS
  def field_class
    @errors[@field].empty? ? nil : 'has_errors'
  end

end
