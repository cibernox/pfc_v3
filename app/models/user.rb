# coding: UTF-8
class User < ActiveRecord::Base
  has_many :authentications, :dependent => :destroy
  has_many :reviews, :dependent => :destroy
  has_many :reviewed_restaurants, :through => :reviews, :source => :restaurant
  has_many :restaurants
  # PHOTOS
  has_many :photos, :as => :attachable
  accepts_nested_attributes_for :photos, :allow_destroy => true
  # DEVISE
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  # VALIDATIONS
  validates :username, :uniqueness => true, :length => { :maximum => 18 }
  # ATTR ACCESSORS
  attr_accessible :email, :password, :password_confirmation, :remember_me, :username,
                  :name, :ap1, :ap2, :born_on, :province, :town, :loved_food, :hated_food,:photos_attributes
  def good_reviews
    reviews.good_ones
  end
  # CLASS METHODS
  def self.filter_fields
    [:email_contains,:username_contains,:province_eq,:town_contains]
  end

  def self.table_fields
    [:id,:email,:username,:name,:ap1,:province,:town]
  end

  def self.fieldsets
    { :account      => [:username, :email],
      :personal     => [:name, :ap1, :ap2, :born_on],
      :geographic   => [:province,:town],
      :contact      => [:loved_food,:hated_food] }
  end

  def self.fieldsets_explanations
    { :account => nil,:personal => nil,:geographic => nil,:contact => nil }
  end

  # OMNIAUTH METHODS
  def apply_omniauth(omniauth)
    self.set_data_from_provider(omniauth)
    authentications.build(:provider => omniauth['provider'],:uid => omniauth['uid'])
  end
  def password_required?                # MODIFICACIÓN PARA QUE NO PIDA EL PASSWORD PARA ACTUALIZAR
    !persisted? || password.present? || password_confirmation.present? || authentications.empty? && super
  end
  def set_data_from_provider(omniauth)  # SOLO SI ESTAMOS REGISTRANDONOS
    omniauth['provider']=='open_id' && email.blank? ? self.email = omniauth['user_info']['email'] : puts('no data to set or unknown provider')
  end

  def nick
    username || "Anonymous"
  end

  def avg_rating(rating = :general_rating)   # Puntuación que otorga de media este usuario a los restaurantes que reseña
    reviews.average(rating)
  end

  def reputacion
    # antes => reviewed_restaurants.inject(0.0) {|memo,obj| memo + obj.avg_rating } / reviewed_restaurants.count
    case (avg_rating/Review.where(:restaurant_id => reviewed_restaurants).where("user_id != #{id}").average(:general_rating))
       when 0.0..0.5 then "Sanguinario"
       when 0.5111111111..0.75 then "Sibarita"
       when 0.7511111111..0.85 then "Exigente"
       when 0.851111111..1.15 then "Medio"
       when 1.151111111..1.25 then "Generoso"
       when 1.251111111..1.5 then "Bonachón"
       when 1.51111111..99 then "Tragaldabas"
       else "Inválido"
    end
  end

  def best_rated_restaurants
    reviews.where('general_rating >= 6').order('general_rating desc').limit(2).includes(:restaurant).map { |review| review.restaurant}
  end

  def recommended_restaurants
    reviews.where('general_rating >= 7').includes(:restaurant).map { |review| review.restaurant}
  end

  def unrecommended_restaurants
    reviews.where('general_rating < 5').includes(:restaurant).map { |review| review.restaurant}
  end


end
