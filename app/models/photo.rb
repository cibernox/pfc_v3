class Photo < ActiveRecord::Base
  belongs_to :attachable, :polymorphic => true
  has_attached_file :image,
                    :styles => {  :mini=>"50x50#",:thumbnail => "100x100#",:small=>"170x170#",:medium_cropped => "250x250#",:medium => "250x250",:big => "450x450",:biggest => "960x960>" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
                    :path => ":attachment/:id/:style.:extension",
                    :bucket => 'vamosdecena'
  validates_attachment_presence :image
  validates_attachment_size :image, :less_than => 3.megabytes
  validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png', 'image/gif']
  before_create :randomize_file_name
  after_save :save_geometry

  def save_geometry
    unless @geometry_saved
      self.medium_geometry = get_geometry(:medium)
      self.big_geometry = get_geometry(:big)
      self.biggest_geometry = get_geometry(:biggest)
      self.original_geometry = get_geometry(:original)
      @geometry_saved = true
      self.save
    end
  end

  def get_geometry(style = :original)
    begin
      Paperclip::Geometry.from_file(image.path(style)).to_s
    rescue
      nil
    end
  end

  def height(style = :original)
    array = get_geometry(style).split('x')
    array[1]
  end
  def width(style = :original)
    array = get_geometry(style).split('x')
    array[0]
  end

  private
  def randomize_file_name
    extension = File.extname(image_file_name).downcase
    self.image.instance_write(:file_name, "#{ActiveSupport::SecureRandom.hex(16)}#{extension}")
  end

end
