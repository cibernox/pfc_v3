# coding: UTF-8
class Course < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :user
  has_many :ticketlines, :dependent => :destroy
  validates :user_id,:name,:price, :presence=>true


  def self.filter_fields
    [:name_contains,:category_eq,:restaurant_name_contains]
  end
  def self.table_fields
    [:id,:name,:category,:price,:restaurant_name]
  end

  def restaurant_name
    "#{restaurant_id}(#{restaurant.name})"
  end

end
