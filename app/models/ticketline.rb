# coding: UTF-8
class Ticketline < ActiveRecord::Base
  belongs_to :review
  belongs_to :course

  def self.filter_fields
    [:course_name_contains,:review_title_contains]
  end

  def self.table_fields
    [:id, :review_title, :course_name, :quantity]
  end
  def review_title
    "#{review_id}(#{review.title})"
  end
  def course_name
    course.name
  end
end
