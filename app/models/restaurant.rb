# coding: UTF-8
class Restaurant < ActiveRecord::Base
  has_many :reviews, :dependent => :destroy
  has_many :reviewers, :through => :reviews, :source => :user
  has_many :courses, :dependent => :destroy
  has_many :photos, :as => :attachable, :dependent=>:destroy   # TODO: Comprobar que dependent=>destroy funciona
  belongs_to :user
  accepts_nested_attributes_for :photos, :allow_destroy => true#, :reject_if => lambda { |t| t['image'].nil? }
  accepts_nested_attributes_for :courses, :allow_destroy => true#, :reject_if => lambda { |t| t['image'].nil? }
  # VALIDACIONES
  validates :name, :town, :street_name,:presence=>true
  validates_exclusion_of :province, :in  => [0], :message=> 'Debes seleccionar uno'
  # RAILS-GEOCODER
  geocoded_by :complete_address, :latitude  => :lat, :longitude => :lng
  # CLASS METHODS
  def self.filter_fields
    [:name_contains,:street_name_contains,:province_eq,:town_contains]
  end
  def self.table_fields
    [:id,:name,:province,:town,:street_name]
  end
  def self.fieldsets
    { :general      => [:name, :province, :town, :street_name, :street_number],
      :geolocation  => [:lat,:lng,:sv_lat,:sv_lng,:sv_heading,:sv_pitch],
      :services     => [:celiac,:adapted,:take_away,:delivery,:credit_card,:menu],
      :contact      => [:tlf,:email,:web],
      :courses      => [] }
  end
  def self.fieldsets_explanations
    { :general      => 'Si conoces la dirección exacta, ponla y dale a localizar. ¿Acertamos? Puedes afinar la posición a mano',
      :geolocation  => 'Haz click derecho para colocar un marcador o entra en StreetView para hacerlo interactivo',
      :services     => nil,
      :contact      => nil,
      :courses      => 'Cuanta más información del menú, mejor. El precio no puede estar en blanco.' }
  end
  # INSTANCE METHODS
  def servicios
    {:celiac=>celiac, :adapted=>adapted, :take_away=>take_away, :delivery=>delivery, :credit_card=>credit_card, :menu=>menu}
  end

  def category_courses(category_id)
    courses.where("category = #{category_id}")
  end

  def address
    [street_name, street_number].delete_if{|e| e.nil?||e.blank? }.join(", ")
  end

  def loc_province
    town.blank? ? '' : "#{town}(#{PROVINCES[province]})"
  end

  def all_photos
    photos + reviews.map { |r| r.photos.map { |p| p } }.flatten
  end

  def avg_rating(rating = :general_rating)
    reviews.average(rating)
  end

  def avg_recomendations
    [:rec_alone,:rec_couple,:rec_child,:rec_friends,:rec_business,:rec_gourmets].map{ |field|  [field, reviews.average(field)] } # works on 3.0.5 o higher
  end

  def near_alternatives
    first_cat ? nearbys(10).where("province = ? AND first_cat = ?",province,first_cat) : []
  end

  def good_reviews_from_users_who_liked_this
    Review.where(:user_id => reviewers.where(:id => reviews.good_ones.map{ |review| review.user })).where("restaurant_id != #{id}").good_ones
  end

  def may_like_you_too
    good_reviews_from_users_who_liked_this.map{ |gr| gr.restaurant }.uniq #sort{|x,y| y[1]<=>x[1]}.map{|r| r[0]}   antes los ordenaba asi
  end

  def calculate_categories
    main_foods = reviews.select('main_food, count(*) as count').group(:main_food).all
    second_foods = reviews.select('second_food, count(*) as count').where('second_food!=0').group(:second_food).all
    mf_count, sf_count = main_foods.sum(&:count).to_f , second_foods.sum(&:count).to_f
    main_foods.map!{|review| [review.main_food, review.count/mf_count] }
    second_foods.map! {|review| [review.second_food, review.count*0.1/sf_count] }
    second_foods.each do |e|
      a = main_foods.assoc(e[0])
      a ? a[1]=a[1]+e[1] : main_foods << [e[0],e[1]]
    end
    ratio = main_foods.sum(&:last)
    main_foods.map!{|e| [e[0],e[1]/ratio]}.sort { |x,y| y[1]<=>x[1]  }
    self.first_cat,self.first_cat_ratio = main_foods[0][0],main_foods[0][1]
    self.second_cat, self.second_cat_ratio = main_foods[1][0], main_foods[1][1] if main_foods[1]
    self.third_cat,self.third_cat_ratio = main_foods[2][0],main_foods[2][1] if main_foods[2]
    save
  end
  def calculate_price
    self.avg_price = reviews.inject(0){|sum,r| sum + r.price if r.price}/reviews.count
    save
  end

end
