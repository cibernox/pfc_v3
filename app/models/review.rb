# coding: UTF-8
class Review < ActiveRecord::Base
  belongs_to  :restaurant
  delegate :name, :town, :province, :photos, :to => :restaurant, :prefix => true
  belongs_to  :user
  has_many    :photos, :as => :attachable, :dependent => :destroy
  has_many    :ticketlines, :dependent => :destroy
  scope       :good_ones, lambda { where('general_rating > 7') }
  accepts_nested_attributes_for :photos, :allow_destroy => true
  accepts_nested_attributes_for :restaurant#, :allow_destroy => true  Creo que aqui no debo dejar destruir
  accepts_nested_attributes_for :ticketlines, :allow_destroy => true
  # VALIDATIONS
  validates :title,:opinion,:main_food,:presence=>true
  validates_exclusion_of :main_food, :in  => [0], :message=> 'Debes seleccionar uno'
  # CALLBACKS
  after_save :'restaurant.calculate_price' , :'restaurant.calculate_categories'
  # TODO: Si en el recuado de precio ponen algo que no sea un numero válido, que salte un error!
  #after_destroy :calculate_categories
#  # CLASS METHODS
  def self.filter_fields
    [:title_contains, :restaurant_name_contains]
  end

  def self.table_fields
    [:id, :restaurant_name, :title, :general_rating]
  end

  def self.fieldsets
    { :opinion          => [:title, :opinion],
      :ratings          => [:food_rating,:service_rating,:local_rating,:clean_rating,:quality_price_rating,:general_rating],
      :recomendations   => [:rec_alone,:rec_couple,:rec_child,:rec_friends,:rec_business,:rec_gourmets],
      :about_your_visit => [:customer_type,:price,:main_food,:second_food],
      :courses          => nil }
  end

  def self.fieldsets_explanations
    { :opinion => nil, :ratings => nil, :recomendations => nil, :about_your_visit => nil,
      :courses => 'Haz doble click(beta) o arrastra platos hasta el ticket. Si no encuentras lo que pediste, añadelo.',
      :photos => nil }
  end
  # INSTANCE METHODS

  def restaurant_name
    "#{restaurant_id}(#{restaurant.name})"
  end

  def ratings
    self.class.fieldsets[:ratings]
  end

  def recomendations
    self.class.fieldsets[:recomendations]
  end

  def recommended_for
    recomendations.select {|r| self[r] > 0  }
  end
  def unrecommended_for
    recomendations.select {|r| self[r] < 0  }
  end

end
