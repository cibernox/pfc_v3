#coding: UTF-8
class ApplicationController < ActionController::Base
  include AdminHelper
  protect_from_forgery
  before_filter :filter_zeros_from_eq_params
  respond_to :html,:js
  def main
    @rad, @lat, @lng = params.delete(:rad).to_f, params.delete(:lat).to_f, params.delete(:lng).to_f
    @rad = 20.0 if @rad.zero?
    @search = Restaurant.search(params[:search])
    @restaurants = @search.relation.includes(:reviews).near([@lat, @lng], @rad * 0.62)
    @restaurants + Restaurant.where(:avg_price=>nil).near([@lat, @lng], @rad * 0.62)
  end

  private
  def redirect_if_not_logged
    redirect_to(new_user_session_path,:notice => t(:logged_only) ) unless current_user
  end
  def redirect_unless_admin
    redirect_to(root_path,:notice=>t(:admins_only)) unless current_user && current_user.admin
  end
end
