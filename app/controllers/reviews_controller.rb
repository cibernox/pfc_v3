# coding: UTF-8
class ReviewsController < ApplicationController
  before_filter :redirect_if_not_logged, :only => [:new, :edit, :update, :destroy]
  before_filter :find_review, :only => [:show, :edit, :update, :destroy]

  def show; end

  def new
    @review = Review.new
    @review.restaurant = Restaurant.find(params[:restaurant_id])
    current_user.reviews.each { |r| redirect_to(edit_restaurant_review_path(r.restaurant, r), :notice=>'Ya tienes un opinion sobre este restaurante. Si quieres cambiar algo, edítala.') if (r.restaurant_id == @review.restaurant_id) }
    redirect_to(root_path, :notice=>"Se ha intentado hacer una review de un restaurante que no existe") unless @review.restaurant
  end

  def edit;  end

  def create
    # TODO: No puedo meter ticketlines desde nueva review, solo desde edit!! Error grave!! Arreglarlo!
    puts "Parametros del create = "+params.to_yaml
    photos = []
    params.each { |name, value| photos << value if name =~ /review_photos/ }
    @review = Review.new(params[:review])
    @review.restaurant = Restaurant.find(params[:restaurant_id])
    @review.user = current_user
    if @review.save
      photos.each do |id|
        @photo = Photo.find(id)
        @photo.attachable_id = @review.id
        @photo.save
      end
      redirect_to(@review.restaurant, :notice => t(:saved_ok))
    else
      backup = @review
      @review = Review.new(:restaurant_id=>backup.restaurant.id)
      backup.errors.each { |attr,msg| @review.errors.add attr, msg  }
      render :new
    end
  end

  def update
    @review.update_attributes(params[:review]) ? redirect_to(@review.restaurant, :notice => 'Opinión actualizada correctamente.') : render(:edit)
  end

  def destroy
    @review.destroy
    redirect_to(reviews_url)
  end

  private
  def find_review
    @review = Review.find(params[:id])
  end
end
