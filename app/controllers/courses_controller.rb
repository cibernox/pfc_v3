# coding: UTF-8
class CoursesController < ApplicationController
  respond_to :html, :js
  before_filter :find_course, :only => [:show, :edit, :update, :destroy]

  def show; end

  def new
    @course = Course.new(:category=>params[:category])
    @review = Review.find(params[:review_id]) if params[:review_id]
    respond_to do |format|
      format.html # new.html.erb
      format.js { params[:restaurant_id].nil? ? render("new_nested") : @course.restaurant = Restaurant.find(params[:restaurant_id]) }
    end
  end

  def edit
    @review = Review.find(params[:review_id]) if params[:review_id]
  end

  def create
    @course = Course.new(params[:course])
    @course.user = current_user
    @course.restaurant = Restaurant.find(params[:restaurant_id])
    @review = Review.find(params[:review_id]) if params[:review_id]
    render(:js=>"alert('El formulario contiene errores')") unless @course.save
  end

  def update
    @review = Review.find(params[:review_id]) if params[:review_id]
    respond_to do |format|
      if @course.update_attributes(params[:course])
        format.html { redirect_to(@course, :notice => t(:updated_ok)) }
        format.js
      else
        format.html { render :edit }
        format.js
      end
    end
  end

  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to(courses_url) }
      format.js
    end
  end

  private

  def redirect_unless_admin
    redirect_to(root_path,:notice=>t(:admins_only)) unless current_user && current_user.admin
  end
  def find_course
    @course = Course.find(params[:id])
  end

end
