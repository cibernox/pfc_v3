class AdminController < ApplicationController
  before_filter :redirect_unless_admin

  def index
    @model= params[:model]
    if @model.match(/^users$|^restaurants$|^reviews$|^courses$|^ticketlines$/i)
      @search = Kernel.const_get(@model.classify).search(params[:search])
      @models = @search.paginate(:per_page=>8,:page=>params[:page] || 1)
    else
      redirect_to root_path
    end
  end

end