class RegistrationsController < Devise::RegistrationsController #  ApplicationController

  def create
    params[:user][:username]= 'Usuario' + User.last.id.to_s
    super
    session[:omniauth] = nil unless @user.new_record?
    unless @user.new_record?
      flash[:notice] = 'username'
      session[:permanent] = true
    end
  end

  private
  def build_resource(*args)
    user = nil
    # TODO: Algunos proveedores devuelven una dirección de email. Si la devuelven, deberia saltarme este paso, pero creo que se comprueba en otro método.
    ## Si tengo en la session un usuario de pasos anteriores y me llega el email de un formulario, pero no llega el password, es que vengo rebotao de un proveedor
    if params[:user] && session[:user] && !params[:user][:password] && !params[:user][:password_confirmation]
        user = session[:user]               ## Recupero el usuario guardado
        user.email = params[:user][:email]  ## Le meto el email (Ya deberia ser un usuario valido si tiene autenticacion externa y email)
        session[:user] = nil                ## Borro al usuario de la session. No pinta nada
    end
    super
    @user = user if user                    ## Si en el paso anterior cree un usuario, quiero usar ese
    if session[:omniauth]
      @user.apply_omniauth(session[:omniauth])
      @user.valid?
    end
  end

end
