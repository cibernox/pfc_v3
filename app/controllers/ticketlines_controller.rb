class TicketlinesController < ApplicationController
  before_filter :find_ticketline,  :only => [:show, :edit, :update, :destroy]

  def show;  end

  def new
    @ticketline = Ticketline.new(:quantity=>1)
    @ticketline.course = Course.find(params[:course_id])
    respond_to do |format|
      format.html # new.html.erb
      format.js
    end
  end

  def edit;  end

  def create
    @ticketline = Ticketline.new(params[:ticketline])
    @ticketline.save ? redirect_to(@ticketline, :notice => 'Linea creada') : render(:new)
  end

  def update
    @ticketline.update_attributes(params[:ticketline]) ? redirect_to(@ticketline, :notice => 'Linea actalizada.') : render(:edit)
  end

  def destroy
    @ticketline.destroy
    redirect_to(ticketlines_url)
  end

  private
  def find_ticketline
     @ticketline = Ticketline.find(params[:id])
  end

end
