class PhotosController < ApplicationController

  def new
    @photo = Photo.new(:attachable_id=>params[:attachable_id].to_i, :attachable_type=>params[:attachable_type] )
    respond_to { |format| format.js }
  end

  def edit
    @photo = Photo.find(params[:id])
  end

  def create
    @photo = Photo.new(params[:photo])
    responds_to_parent do
      @photo.save ? render(:create) : render(:js=>"alert('error')")
      # respondo con la accion.js, y hago lo que me de la gana.Pero igual deberia hacer que cada formulario tuviese su propio iframe
      # para poder subir varios fotos sin tener que guardas las anteriores
    end
  end

  def update
    @photo = Photo.find(params[:id])
    @photo.update_attributes(params[:photo]) ? redirect_to(:id=>nil, :notice => t(:updated_ok)) : render(:edit)
  end

  def destroy
    puts params.to_yaml
    @photo = Photo.find(params[:id])
    @id = @photo.id
    @photo.destroy
    respond_to do |format|
      #format.html { redirect_to(photos_url) }
      format.js
    end
  end

  private
  # Este método hace la magia de las relaciones polimorficas. Busca el elemento asociado en función de si hay un parametro
  # llamado user_id (tabla Users), restaurant_id (Restaurants) o review_id (Reviews)
  def find_attachable
    params.each { |name, value| return $1.classify.constantize.find(value) if name =~ /(.+)_id$/ }
    nil
  end
end
