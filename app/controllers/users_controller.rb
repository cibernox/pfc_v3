# coding: UTF-8
class UsersController < ApplicationController
  before_filter :find_user, :only => [:show, :edit, :update]
  before_filter :redirect_if_not_logged, :redirect_if_not_current, :only => [:edit, :update]
  # TODO: Yo no veo como llegar a esta acción. Y menos aun como llegar a el JS. Ni puta idea. Investigarlo mejor, y sino, borrar
  def new
    @user = User.new
    @num_photos = params[:num_photos] ? [params[:num_photos].to_i, 6].min : 0 #maximo 6 fotos
    @num_photos.times { @user.photos.build }
    respond_to do |format|
      format.html { redirect_to new_user_registration_path }
      format.js
    end
  end

  def show; end

  def edit
    @num_photos = params[:num_photos] ? [params[:num_photos].to_i, 6].min : @user.photos.count #maximo 6 foto
  end

  def update
    @user.update_attributes(params[:user]) ? redirect_to(current_user, :notice => t(:updated_ok)) : redirect_to( edit_user_path(current_user), :notice => t(:cant_update) )
  end

  private
  def find_user
    @user = User.find(params[:id])
  end
  def redirect_if_not_current
    redirect_to(new_user_session_path, :notice=>t(:logged_only)) unless @user==current_user
  end


end
