class RestaurantsController < ApplicationController
  before_filter :redirect_if_not_logged, :only => [:new, :edit, :update, :destroy]
  before_filter :set_courses_author, :only => [:create,:update]
  before_filter :find_restaurant, :only => [:show, :edit, :update, :destroy]
  def show
    render("infobox",:layout=>nil) if params[:infobox]
    @restaurant.photos.each { |photo| @main_photo = photo if photo.id==params[:photo_id].to_i }
  end

  def new
    @restaurant = Restaurant.new
  end

  def edit
    @num_photos = params[:num_photos] ? [params[:num_photos].to_i, 6].min : @restaurant.photos.count  #maximo 6 fotos
    @num_courses = params[:num_courses] ? params[:num_courses].to_i : @restaurant.courses.count
  end

  def create
    photos = []
    params.each { |name, value| photos << value if name =~ /restaurant_photos/ }
    @restaurant = Restaurant.new(params[:restaurant])
    @restaurant.user = current_user
    if @restaurant.save
      photos.each do |id|
        @photo = Photo.find(id)
        @photo.attachable_id = @restaurant.id
        @photo.save
      end
      redirect_to(@restaurant, :notice => t(:saved_ok))
    else
      errors = @restaurant.errors
      @restaurant = Restaurant.new
      errors.each { |attr,msg| @restaurant.errors.add attr, msg  }
      render :new
    end
  end

  def update
    if @restaurant.update_attributes(params[:restaurant])
      respond_to do |format|
        format.html { redirect_to(@restaurant, :notice => t(:updated_ok)  ) }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @restaurant.destroy
    redirect_to(restaurants_url)
  end

  private
  def set_courses_author
    params[:restaurant][:courses_attributes].each { |c| c[1][:user_id]=current_user.id.to_s } if params[:restaurant] && params[:restaurant][:courses_attributes]
  end
  def find_restaurant
    @restaurant = Restaurant.find(params[:id])
  end
end
