require 'compass'
require 'compass/app_integration/rails'
Sass::Plugin.options[:never_update] = true
Compass::AppIntegration::Rails.initialize!
