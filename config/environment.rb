# coding: UTF-8
# Load the rails application
require File.expand_path('../application', __FILE__)
require 'will_paginate'

# Initialize the rails application
V3::Application.initialize!

# Constantes

PROVINCES = {0 => 'Provincia',1 => 'Albacete',2 => 'Alicante',3 => 'Almería',4 => 'Araba',5 => 'Asturias',6 => 'Ávila',7 => 'Badajoz',8 => 'Baleares',
  9 => 'Barcelona',10 => 'Bizkaia',11 => 'Burgos',12 => 'Cáceres',13 => 'Cádiz',14 => 'Cantabria',15 => 'Castellón',16 => 'Ceuta',17 => 'Ciudad Real',
  18 => 'Córdoba',19 => 'Cuenca',20 => 'Girona',21 => 'Granada',22 => 'Guadalajara',23 => 'Gipuzkoa',24 => 'Huelva',25 => 'Huesca',26 => 'Jaén',
  27 => 'La Coruña',28 => 'La Rioja',29 => 'Las Palmas',30 => 'León',31 => 'Lleida',32 => 'Lugo',33 => 'Madrid',34 => 'Málaga',35 => 'Melilla',
  36 => 'Murcia',37 => 'Navarra',38 => 'Ourense',39 => 'Palencia',40 => 'Pontevedra',41 => 'Salamanca',42 => 'Segovia',43 => 'Sevilla',44 => 'Soria',
  45 => 'Tenerife',46 => 'Tarragona',47 => 'Teruel',48 => 'Toledo',49 => 'Valencia',50 => 'Valladolid',51 => 'Zamora',52 => 'Zaragoza'}
FOOD_TYPES = {0 =>'Tipo de comida',1 => 'Casera', 2 => 'China', 3 => 'Fusión',4 => 'Griega',5 => 'Hindú',6 => 'Innovadora',7 => 'Italiana',
  8 => 'Japonesa',9 => 'Mejicana',10 => 'Rápida',11 => 'Tapas',12 => 'Tradicional',13 => 'Turca/Kebab' }
FOOD_CATEGORIES = { 0=>'Categoría de comida',1 => 'Bebidas',2=>'Bocadillos',3=>'Carnes',4=>'Huevos',5=>'Mariscos',6=>'Otros',7=>'Pastas',8=>'Pizzas',9=>'Pescados',
                    10=>'Postres',11=>'Verduras',12=>'Vinos/Alcohol' }
CUSTOMER_TYPES = {0 =>'Ir solo',1 => 'Ir en pareja', 2 => 'Ir con niños', 3 => 'Ir con amigos', 4 => 'Comida de negocios', 5 => 'Los más gourmets'}
PRICES = {0=>'0 €',10=>'10 €',20=>'20 €',30=>'30 €',40=>'40 €',50=>'50 €',100=>'100 €'}
RADIOS = {0=>'-',0.05=>'50 m',0.1=>'100 m',0.2=>'200 m',0.5=>'500 m',0.75=>'750 m',1=>'1 km',2=>'2 km',5=>'5 km',10=>'10 km'}
ZOOMS = {0=>0.4055, 1=>0.4055,  2=>0.4055, 3=>0.4055, 4=>0.4055, 5=>0.4055, 6=>0.4055, 7=>0.4055, 8=>0.4055, 9=>0.4055, 10=>29.4055,
         11=>14.7027, 12=>7.3530, 13=>3.6765, 14=>1.8383, 15=>0.9192, 16=>0.45960, 17=>0.2298, 18=>0.1148, 19=>0.05745, 20=>0.05745, 21=>0.05745}
COLLECTIONS = { :province => PROVINCES, :main_food => FOOD_TYPES, :second_food => FOOD_TYPES,:hated_food=> FOOD_TYPES,
                :loved_food => FOOD_TYPES, :customer_type=> CUSTOMER_TYPES, :food_categories=>FOOD_CATEGORIES, :category=>FOOD_CATEGORIES}
